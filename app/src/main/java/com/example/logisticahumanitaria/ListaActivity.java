package com.example.logisticahumanitaria;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.Device;
import database.EstadoPersistencia;
import database.Estados;
import database.ProcesosPHP;

public class ListaActivity extends ListActivity implements Response.Listener<JSONObject>,Response.ErrorListener{

    private EstadoPersistencia estadosPersistencia;

    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> listaEstados;
    private String serverip ="https://riverajesusgpe98.000webhostapp.com/WebService/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        Button btnNuevo = findViewById(R.id.btnNuevo);

        if(isNetworkAvailable()){
            listaEstados = new ArrayList<Estados>();
            request = Volley.newRequestQueue(context);
            consultarTodosWebService();
        }else{
            estadosPersistencia = new EstadoPersistencia(this);
            estadosPersistencia.openDatabase();
            ArrayList<Estados> estados = estadosPersistencia.allContactos();
            MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.layout_estado, estados);
            setListAdapter(adapter);
            //Toast.makeText(getApplicationContext(), "Entra:" + estados.isEmpty(),Toast.LENGTH_SHORT).show();
        }


        //NUEVO
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    public void consultarTodosWebService(){
        String url = serverip + "wsConsultarTodos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
    }
    @Override
    public void onResponse(JSONObject response) {
        Estados estado = null;
        JSONArray json = response.optJSONArray("estados");
        try {
            for(int i=0;i<json.length();i++){
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.setId_estado(jsonObject.optInt("id"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("idDispositivo"));
                listaEstados.add(estado);
            }
            MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.layout_estado,listaEstados);
            setListAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }



    class MyArrayAdapter extends ArrayAdapter<Estados> {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = view.findViewById(R.id.lblNombreEstado);
            Button modificar = view.findViewById(R.id.btnModificar);
            Button borrar = view.findViewById(R.id.btnBorrar);

            lblNombre.setText(objects.get(position).getNombre());

            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isNetworkAvailable()){
                        AlertDialog.Builder confirmar = new AlertDialog.Builder(ListaActivity.this);
                        confirmar. setTitle("Confirmación eliminación").setMessage("¿Está seguro de eliminar este Estado?");
                        confirmar.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Estados estado = new Estados();
                                estado.setIdMovil(Device.getSecureId(ListaActivity.this));
                                php.setContext(ListaActivity.this);
                                php.borrarEstadoWebService((objects.get(position).getId_estado()),Device.getSecureId(ListaActivity.this));

                                estadosPersistencia = new EstadoPersistencia(ListaActivity.this);
                                estadosPersistencia.openDatabase();
                                estadosPersistencia.deleteContacto(objects.get(position).getId_estado());
                                estadosPersistencia.close();

                                objects.remove(position);
                                notifyDataSetChanged();

                                Toast.makeText(getApplicationContext(), "Contacto eliminado con exito", Toast.LENGTH_SHORT).show();
                            }
                        });
                        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "Acción cancelada", Toast.LENGTH_SHORT).show();
                            }
                        });
                        confirmar.show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Requiere conexión",Toast.LENGTH_SHORT).show();
                    }

                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("estados", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }


}

