package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class EstadoPersistencia {
    Context context;
    RegistroDbHelper mDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]{
            DefinirTabla.Estados._ID,
            DefinirTabla.Estados.COLUMN_NAME_NOMBRE
    };

    public EstadoPersistencia(Context context) {
        this.context = context;
        mDbHelper = new RegistroDbHelper(this.context);
    }
    public void openDatabase(){
        db = mDbHelper.getWritableDatabase();
    }
    public long insertContacto(Estados c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, c.getNombre());
        return db.insert(DefinirTabla.Estados.TABLE_NAME, null, values);
        //regresa el id insertado
    }
    public long updateContacto(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, c.getNombre());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estados.TABLE_NAME , values,
                DefinirTabla.Estados._ID + " = " + id,null);
    }
    public int deleteContacto(long id){
        return db.delete(DefinirTabla.Estados.TABLE_NAME,DefinirTabla.Estados._ID + "=?",
                new String[]{ String.valueOf(id) });
    }
    private Estados readContacto(Cursor cursor){
        Estados c = new Estados();
        c.setId_estado(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        return c;
    }
    public Estados getEstado(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estados.TABLE_NAME, columnsToRead, DefinirTabla.Estados._ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);
        c.moveToFirst();
        Estados contacto = readContacto(c);
        c.close();
        return contacto;
    }

    public ArrayList<Estados> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estados.TABLE_NAME, columnsToRead, null, null, null, null, null);
        ArrayList<Estados> estados = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readContacto(cursor);
            estados.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return estados;
    }
    public void close(){
        mDbHelper.close();
    }
}
