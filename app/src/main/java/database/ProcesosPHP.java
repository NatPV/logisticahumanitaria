package database;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> estado = new ArrayList<Estados>();
    private String serverip = "https://riverajesusgpe98.000webhostapp.com/WebService/";

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    public void insertEstadoWebService(Estados c) {
        String url = serverip + "wsRegistro.php?nombre=" + c.getNombre()
                + "&idDispositivo=" + c.getIdMovil();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void actualizarEstadoWebService(Estados c, int id) {
        String url = serverip + "wsActualizar.php?id=" + id
                + "&nombre=" + c.getNombre()
                + "&idDispositivo=" + c.getIdMovil();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void borrarEstadoWebService(int id, String idM) {

            String url = serverip+"wsEliminar.php?id="+id+"&idDispositivo="+idM;
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            try {
                request.add(jsonObjectRequest);
            }catch (Exception e){

            }


    }

    public void mostrarEstadoWebService(Estados c) {
        String url = serverip + "wsConsultarUno.php?idDispositivo=" + c.getIdMovil()
                + "&nombre=" + c.getNombre();

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void mostrarEstadoTodosWebService() {
        String url = serverip + "wsConsultarTodos.php";
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
    }
}